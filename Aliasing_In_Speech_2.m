clear all;close all
clc;
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Feb. 2018
% Notice the effect of decimation without/with LPF
FILE='speech_dft.wav';
[inp,Fs]=audioread(FILE);
inp=inp(11000:60000);
DecFactor=10; % Play with this.....
DecFactor=input('Please enter DECIMATION FACTOR (2-10)....');
InpDec=inp(1:DecFactor:end);
% Decimation after LPF
B=fir1(64,1/DecFactor); % Cutoff at pi/L
A=1;
freqz(B,A,128);
title('Designed LPF for decimation');
InpDecFiltered= filter(B,A,inp);
InpDecFiltered=InpDecFiltered(1:DecFactor:end);
%-------------------------------------------
disp('Playing sounds now'); 
soundsc(inp,Fs);
input('Original input, ENTER a key... ');
soundsc(InpDec,Fs/DecFactor);
input('Decimated and aliased speech,  ENTER a key...');
soundsc(InpDecFiltered,Fs/DecFactor);
input('LPF+ decimated speech, NO Aliasing error!, ENTER a key......');
%
%------------------------------------------------------
% Plot the spectrograms
N=256;
win=hamming(N);
NOVERLAP=N/2;
NFFT=1024;
figure;
subplot(311)
L=18000;
[y,f,t,p] = spectrogram(inp(1:L),win,NOVERLAP,NFFT,Fs);
surf(t,f,10*log10(abs(p)),'EdgeColor','none');
axis xy; axis tight; colormap(jet); view(0,90);
xlabel('Time');ylabel('Frequency (Hz)');
title('The ORINGINAL Specrogram');
% Now plot the DECIMATED version
N1=fix(N/DecFactor);
win1=hamming(N1);
NOVERLAP1=fix(N1/2);
NFFT1=1024;
L1=18000/DecFactor;
subplot(312)
[y,f,t,p] = spectrogram(InpDec(1:L1),win1,NOVERLAP1,NFFT1,Fs/DecFactor);
surf(t,f,10*log10(abs(p)),'EdgeColor','none');
axis xy; axis tight; colormap(jet); view(0,90);
xlabel('Time');ylabel('Frequency (Hz)');
title('The DECIMATED/Aliased Specrogram, WHERE ERROR IS THE MOST?');
%-------------------------------------------
subplot(313)
[y,f,t,p] = spectrogram(InpDecFiltered(1:L1),win1,NOVERLAP1,NFFT1,Fs/DecFactor);
surf(t,f,10*log10(abs(p)),'EdgeColor','none');
axis xy; axis tight; colormap(jet); view(0,90);
xlabel('Time');ylabel('Frequency (Hz)');
title(['LPF+ DECIMATED by ',num2str(DecFactor),'  Specrogram']);

return;

% Written by: Hamid Sheikhzadeh
% Demo program for ideal LPF, DSP B.Sc course, March, 2018
% Notes: observe that:
% 1. As time length increases, the filter transition becomes sharper in freq.
% 2. However, we have the same -21 dB cut-off loss, not more!
% 3. Increasing length beyound certain length (e.g 50 here) does NOT HELP, 
% the filter amp. resp. is masked by the quantization noise!
clear all;
close all;clc
wc=1/2; % LPF cut-off freq., pi/2
TimeLen=1024;
EndPoint= 10:20:90;
FilterNum=1;
for endpoints= EndPoint
    n= -endpoints:1:endpoints;
    x=sinc(wc*n); % sin(pi*x)/(pi*x)
    x= (-1).^n .*x; % Modulate by pi, make it highpass
    figure(1);
    subplot(length(EndPoint),1,FilterNum);
    plot(n,x,'.-');
    grid on;
    title(['Impulse Response of the ideal HPF, Time Range= -',num2str(endpoints),':',num2str(endpoints)]);
    [H,w]=freqz(x,sum(x),1024);
    figure(2)
    subplot(length(EndPoint),1,FilterNum)
    plot(w/pi,20*log10(abs(H)) );
    grid on; 
    title(['Freq. Resp. of the ideal HPF, Time Range= -',num2str(endpoints),':',num2str(endpoints)]);
    xlabel('w/pi');
    axis tight
    FilterNum=FilterNum+1;
end;



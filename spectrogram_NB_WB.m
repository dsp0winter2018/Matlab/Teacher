clear all;close all
clc;
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Feb. 2018
% To demonstrate effect of time-window
FILE='speech_dft.wav';
[inp,Fs]=audioread(FILE);
inp=inp(11000:40000);
%
%------------------------------------------------------
% Plot the spectrograms
N=128;
win=hamming(N);
NOVERLAP=N/2;
NFFT=1024;
figure;
subplot(211)
L=18000;
[y,f,t,p] = spectrogram(inp(1:L),win,NOVERLAP,NFFT,Fs);
surf(t,f,10*log10(abs(p)),'EdgeColor','none');
axis xy; axis tight; colormap(jet); view(0,90);
xlabel('Time');ylabel('Frequency (Hz)');
title(['Wideband Specrogram, window length= ',num2str(N)]);
% Now plot the DECIMATED version
N1=512;
win1=hamming(N1);
NOVERLAP1=N1/2;
NFFT1=2048;
L1=18000;
subplot(212)
[y,f,t,p] = spectrogram(inp(1:L1),win1,NOVERLAP1,NFFT1,Fs);
surf(t,f,10*log10(abs(p)),'EdgeColor','none');
axis xy; axis tight; colormap(jet); view(0,90);
xlabel('Time');ylabel('Frequency (Hz)');
title(['Wideband Specrogram, window length= ',num2str(N1)]);
return;

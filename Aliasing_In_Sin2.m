clear all;close all
clc;
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Feb. 2018
% Notice the effect of decimation on sines
w0=2/3; % aperiodic sine
w1=1/3+1/10;
N=1024;
t=0:1:N-1;
Fs=1;
inp=sin(w0*t)+10*sin(w1*t);
DecFactor=[1, 5, 10];% Play with this.....
NumFigs=length(DecFactor);
Num=1;
for Dec=DecFactor
    InpDec=inp(1:Dec:end);
    tDec=t(1:Dec:end);
    figure(1);
    subplot(NumFigs,1,Num);
    plot(tDec,InpDec,'b.-');
    grid on;
    %------------------------------
    title(['sine with DECIMATION= ',num2str(Dec)]);
    figure(2);
    subplot(NumFigs,1,Num);
    [H_InpDec,f2Hz]=freqz(InpDec,1,N,Fs/Dec);
    plot(f2Hz,20*log10(abs(H_InpDec*Dec)),'r.-');
    grid on;
    xlabel('Freq. (Hz)');
    title(['DTFT mag. with DECIMATION of ',num2str(Dec)]);
    Num=Num+1;
end;


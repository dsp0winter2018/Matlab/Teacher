close all;
clear all; clc
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Oct. 2017
% Effect of COMPRESSION and EXPANSION on DTFT
N=256;
W= linspace(-pi*.99,pi*.99,N);
%
t= -5:.1:5; 
L=length(t);
inp=sinc(t*4);
[H]=freqz(inp,1,W);
H=H./max(norm(H));
figure
plot(W,20*log10(abs(H)),'b.-');grid on
title('Amp Resp,(dB) of Input (Blue), Decimated (Red), Expanded (Green) ')
%%
hold on
M=2;
inp_dec=inp(1:M:end);
t_dec=t(1:M:end);
[H_dec]=freqz(inp_dec,1,W);
H_dec=H_dec/max(norm(H_dec));
plot(W,20*log10(abs(H_dec)),'r.-');grid on
%% 
I=3;
inp_intp=zeros(1,length(inp)*I);
inp_intp(1:I:end)=inp;
t_intp= -5:.1/I:5;
inp_intp=inp_intp(1:length(t_intp));
[H_intp]=freqz(inp_intp,1,W);
H_intp=H_intp/max(norm(H_intp));
%
hold on
plot(W,20*log10(abs(H_intp)),'g.-');grid on
%%
%--------------------------------------
figure
plot(t,inp,'b.-');
hold on
stem(t_intp,inp_intp,'g*');
grid on
title('Time wave of Input (Blue), Decimated (Red), Expanded (Green)');
plot(t_dec,inp_dec,'ro');grid on
%-------------------------------------------------------

return




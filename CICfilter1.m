% Written by: Hamid Sheikhzadeh
% Demo program for ideal LCascaded Integrator Comb (CIC) filter March, 2018
% 
clear all;
close all;clc
FiltOrder=8;
B=[1 zeros(1,FiltOrder-2),1]; % 1 - z^(-N), comb filter
stem(B)
A=[1 -1]; % 1/( 1-z^(-1) )  Integrator
%
Nfft=512;
w=linspace(.01,pi-0.01,Nfft); % Avoid w=0, and w=pi!
figure;
H_Comb= freqz(B,1,w);
H_Integ=freqz(1,A,w);
H_CIC=freqz(B,A,w);
plot(w,20*log10(abs(H_Comb)),'b.-');
grid on;hold on
 plot(w,20*log10(abs(H_Integ)),'r.-');
 plot(w,20*log10(abs(H_CIC)),'g.-');
%
title('Mag. Resp: Comb, Integrator, and CIC (same as MA) Filters');
legend('COMB','Integrator','CIC');
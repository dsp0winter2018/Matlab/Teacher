clear all;close alllinspace
clc;
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Feb. 2018
% Notice the effect of decimation on sines
w0=3/4; % aperiodic sine
N=256;
t=0:1:N-1;
Fs=1;
inp=sin(w0*t);
figure(1);
plot(t,inp,'.-');
hold on;grid on;
for DecFactor=[ 6, 8,12, 20] % Play with this.....
    InpDec=inp(1:DecFactor:end);
    tDec=t(1:DecFactor:end);
    figure(1);
    plot(tDec,InpDec,'r.-');
    %------------------------------------------------------
    figure(2);
    [H_Inp,f1Hz]=freqz(inp,1,N,Fs);
    plot(f1Hz,20*log10(abs(H_Inp)),'b.-');
    grid on
    hold on;
    [H_InpDec,f2Hz]=freqz(InpDec,1,N,Fs/DecFactor);
    plot(f2Hz,20*log10(abs(H_InpDec*DecFactor)),'r.-');
   end;
figure(1);
title('Input sine (BLUE) and decimated versions (RED)');
figure(2);
 xlabel('Freq. (Hz)');
 title('DTFT mag. of input (BLUE) and decimated versions (RED)');